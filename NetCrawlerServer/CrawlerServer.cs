﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using NetCrawlerCommon.DBContent;
using LinqToDB;

namespace NetCrawlerServer
{
    public partial class CrawlerServer : ServiceBase
    {
        public CrawlerServer()
        {
            InitializeComponent();
        }
        /// <summary>
        /// 服务入口
        /// </summary>
        /// <param name="args"></param>
        protected override void OnStart(string[] args)
        {
            using (var db = new SqlDBContent())
            {               
                db.SysLogs.Value(a => a.CreateTime, DateTime.Now)
                    .Value(a => a.LogsContent, "爬取服务启动")
                    .Value(a => a.LogsType, 1)
                    .Value(a => a.Remark, "服务启动/关闭")
                    .Insert();
            }
            //开始
            CrawlerServerHelper.Onstart();
        }
        /// <summary>
        /// 出口
        /// </summary>
        protected override void OnStop()
        {
            using (var db = new SqlDBContent())
            {                
                db.SysLogs.Value(a => a.CreateTime, DateTime.Now)
                    .Value(a => a.LogsContent, "爬取服务关闭")
                    .Value(a => a.LogsType, 1)
                    .Value(a => a.Remark, "服务启动/关闭")
                    .Insert();
            }
        }
    }
}
