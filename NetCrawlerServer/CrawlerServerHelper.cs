﻿using NetCrawlerCommon.CommonHelp;
using NetCrawlerCommon.DBContent;
using NetCrawlerCommon.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace NetCrawlerServer
{
    public class CrawlerServerHelper
    {
        /// <summary>
        /// 任务数 建议每天爬取最多5000条列表数据
        /// </summary>
        private static int taskcounts;
        /// <summary>
        /// 
        /// </summary>
        private const int pageSize = 500;
        /// <summary>
        /// 定时器
        /// </summary>
        private static Timer timer;
        public static void Onstart()
        {

            if (isCrawlerDetail() > 0)
            {
                CrawlerDetail();
            }
            //注册计时器的事件
            timer = new Timer();
            timer.Elapsed += new ElapsedEventHandler(Ontimerevent);

            //设置时间间隔为1天，覆盖构造函数设置的间隔
            timer.Interval = 1000*60*60*24;

            //设置是执行一次（false）还是一直执行(true)，默认为true
            timer.AutoReset = true;

            //开始计时
            timer.Enabled = true;

        }
        /// <summary>
        /// 如果大于0  爬取列表数据
        /// </summary>
        /// <returns></returns>
        private static int isCrawlerList()
        {
            //改数据页面爬取一次获得
            int pagenumbers = 206458;
            int result = 0;
            using (var db = new SqlDBContent())
            {
                result = pagenumbers - db.PunishmentInformation.Count();
            }
            return result;
        }
        /// <summary>
        /// 如果大于0  爬取详情数据
        /// </summary>
        /// <returns></returns>
        private static int isCrawlerDetail()
        {
            int result = 0;
            using (var db = new SqlDBContent())
            {
                result = db.PunishmentInformation.Where(a => a.Isscanf == 0).Count();
            }
            return result;
        }
        /// <summary>
        /// 分策略爬取
        /// </summary>
        /// <param name="pagenumber">待爬取的总跳数</param>
        private static void CrawlerList(int pagenumber)
        {
            taskcounts = pagenumber / pageSize + 1;
            int lastList = pagenumber % pageSize;
            int MaxpageID = 0;
            using (var db = new SqlDBContent())
            {
                MaxpageID = db.PunishmentInformation.LastOrDefault().pageID.Value;
            }
            TaskSupervisor t = new TaskSupervisor(taskcounts);
            var page = new BasePage();
            for (int i = 0; i < taskcounts; i++)
            {
                page.pageNo = MaxpageID + i + 1;

                page.pageSize = i == taskcounts - 1 ? lastList : pageSize;
                t.Add(Task.Factory.StartNew(CrawlerBusiness.getAdministrativeBypage, page));
            }
            t.StartAll();
        }
        /// <summary>
        /// 单线程 爬取详情数据 
        /// </summary>
        private static void CrawlerDetail()
        {
            TaskSupervisor t = new TaskSupervisor(1);
            t.Add(Task.Factory.StartNew(CrawlerBusiness.getAdministrativeSanctionDetail, 0));
            t.StartAll();
        }

        private static void Ontimerevent(object source, ElapsedEventArgs e)
        {
            int Crawlernumber = isCrawlerList();
            if (Crawlernumber > 0)
            {
                CrawlerList(Crawlernumber);
            }            
        }

    }
}
