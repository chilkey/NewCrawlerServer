﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetCrawlerCommon.CommonHelp
{
    /// <summary>
    /// 计数器辅助类
    /// </summary>
    class Counter : ICounter
    {
        private int _count = 0;
        public int Count { get { return _count; } }
        /// <summary>
        /// 计数器+1
        /// </summary>
        public void Increment()
        {
            _count++;
        }
        /// <summary>
        /// 计数器-1
        /// </summary>
        public void Decrement()
        {
            _count--;
        }
        /// <summary>
        /// 重置计数器
        /// </summary>
        public void InitCounter()
        {

            _count = 0;
        }
    }
}
