﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace NetCrawlerCommon.CommonHelp
{
    public class IpHelper
    {
        public string GetIP()
        {
            StringBuilder ips = new StringBuilder();
            string name = Dns.GetHostName();
            IPAddress[] ipadrlist = Dns.GetHostAddresses(name);
            foreach (IPAddress ipa in ipadrlist)
            {
                if (ipa.AddressFamily == AddressFamily.InterNetwork)
                    ips.AppendFormat("{0},", ipa.ToString());
            }
            return ips.ToString();
        }
    }
}
