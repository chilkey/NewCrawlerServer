﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetCrawlerCommon.CommonHelp
{
    using System.Threading;

    /// <summary>
    /// Base 基础行为
    /// </summary>
    public class Base
    {
        /// <summary>
        /// cancelToken
        /// </summary>
        public static CancellationTokenSource CancelTokenSource = new CancellationTokenSource();

        /// <summary>
        /// 进度通知
        /// </summary>
        public static Progress<string> Progress = new Progress<string>();

    }
}
