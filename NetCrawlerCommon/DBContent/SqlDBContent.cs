﻿using LinqToDB;
using LinqToDB.DataProvider;
using LinqToDB.DataProvider.SqlServer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace NetCrawlerCommon.DBContent
{


    /// <summary>
    /// 
    /// </summary>
   public class SqlDBContent: LinqToDB.Data.DataConnection
    {
        public SqlDBContent() : base(GetDataProvider(), GetConnection()) { }



        private static IDataProvider GetDataProvider()
        {
            return new SqlServerDataProvider("", SqlServerVersion.v2012);
        }
        private static IDbConnection GetConnection()
        {
            LinqToDB.Common.Configuration.AvoidSpecificDataProviderAPI = true;
            var dbConnection = new SqlConnection(@"Data Source=.;Initial Catalog=NetCrawlerDB;Persist Security Info=True;User ID=sa;Password=Cjian199123;");
            return dbConnection;
        }


        public ITable<BJZX> BJZX { get { return GetTable<BJZX>(); } }

        public ITable<BJZXDetail> BJZXDetail { get { return GetTable<BJZXDetail>(); } }

        public ITable<PunishmentInformation> PunishmentInformation { get { return GetTable<PunishmentInformation>(); } }

        public ITable<SysLogs> SysLogs { get { return GetTable<SysLogs>(); } }
    }
   
}
