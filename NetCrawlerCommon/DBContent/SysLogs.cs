﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB.Mapping;

namespace NetCrawlerCommon.DBContent
{
    [Table(Name = "SysLogs")]

    public class SysLogs
    {
        [PrimaryKey, Identity]
        public int ID { get; set; }
   
        [Column(Name = "CreateTime")]
        public DateTime? CreateTime { get; set; }

        [Column(Name = "LogsContent")]
        public string LogsContent { get; set; }

        [Column(Name = "LogsType")]
        public int? LogsType { get; set; }

        [Column(Name = "Remark")]
        public string Remark { get; set; }
    }
}
