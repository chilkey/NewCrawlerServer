﻿
using NetCrawlerCommon.DBContent;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;


namespace NetCrawlerCommon.Net
{
    public class NetCrawlerContent
    {

        /// <summary>
        /// 读取URL地址内容
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public  string GetURLContent(string url,  RequestBody body)
        {
            string result = string.Empty;
            try
            {
                url = url + "&pageNo=" + body.pageNo+ "&pageSize=" + body.pageSize+ "&SelectPageSize="+ body.SelectPageSize;
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.154 Safari/537.36 LBBROWSER";
                request.Method = "POST";
                string str = JsonConvert.SerializeObject(body);
       
                byte[] btBodys = Encoding.UTF8.GetBytes(str);
                request.ContentLength = btBodys.Length;
               // request.GetRequestStream().Write(btBodys, 0, btBodys.Length);
                using (var dataStream = request.GetRequestStream())
                {
                    dataStream.Write(btBodys, 0, btBodys.Length);
                }
               
                using (WebResponse response = request.GetResponse())
                {

                    Stream dataStream = response.GetResponseStream();

                    StreamReader reader = new StreamReader(dataStream, Encoding.UTF8);

                    result = reader.ReadToEnd();
                }
            }
            catch(Exception ex)
            {

            }
            return result;
        }
        /// <summary>
        /// 读取URL地址内容
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public string GetURLContent(string url)
        {
            string result = string.Empty;
            try
            {
               
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                request.UserAgent = "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.154 Safari/537.36 LBBROWSER";
                request.Method = "GET";

                //request.ServicePoint.
                //request.Timeout = 

                using (WebResponse response = request.GetResponse())
                {

                    Stream dataStream = response.GetResponseStream();

                    StreamReader reader = new StreamReader(dataStream, Encoding.UTF8);

                    result = reader.ReadToEnd();
                }
            }
            catch (Exception ex)
            {
                //using (FileStream fsRead = new FileStream(@"D:\1.txt", FileMode.Create))
                //{
                //    char[] strs =new char[ex.Message.Length];
                    
                //    fsRead.Write()


                //}
            }
            return result;
        }      
    }
}
