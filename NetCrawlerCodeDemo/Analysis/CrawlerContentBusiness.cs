﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCrawlerCodeDemo
{
    using System.Collections;
    using System.Threading;



    /// <summary>
    /// CrawlerBehaviorBusiness 爬虫行为逻辑
    /// </summary>
    public class CrawlerContentBusiness : BehaviorTemplate
    {
        /// <summary>
        /// 待下载的url列表
        /// </summary>
        private Queue WaitUrlQueue = new Queue();

        /// <summary>
        /// 已存在的url列表
        /// </summary>
        private List<string> ExistList = new List<string>();

        /// <summary>
        /// 任务管理器
        /// </summary>
        private TaskSupervisor TaskSupervisor { get; set; }

        /// <summary>
        /// 默认构造函数
        /// </summary>
        private CrawlerContentBusiness() { }

        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="maxTaskNum">最大任务数</param>
        /// <param name="urlList">待下载</param>
        public CrawlerContentBusiness(int maxTaskNum, Queue urlList)
        {
            WaitUrlQueue = urlList;
            TaskSupervisor = new TaskSupervisor(maxTaskNum);
        }

        /// <summary>
        /// 开始分析
        /// </summary>
        public override async Task Action()
        {
            try
            {
                NetCrawlerContent downloadHelper = new NetCrawlerContent();
                while (!Base.CancelTokenSource.IsCancellationRequested)
                {
                    TaskSupervisor.DisposeInvalidTask();

                    if (WaitUrlQueue.Count < 1)
                    {
                       
                        await Task.Delay(2000);
                        continue;
                    }
                    string url = Dequeue();
                    if (!ExistList.Contains(url))
                    {                      
                        Task task = new Task(delegate { downloadHelper.GetURLContent(url,new DBContent.RequestBody() {  pageNo = 1 , pageSize = 100}); }, Base.CancelTokenSource.Token);
                        bool result = TaskSupervisor.Add(task);
                        if (!result)
                        {                           
                            await Task.Delay(2000);
                            WaitUrlQueue.Enqueue(url);
                            continue;
                        }
                        ExistList.Add(url);
                    }
                }

                TaskSupervisor.DisposeInvalidTask();
            }
            catch (Exception ex)
            {

                throw;
            }

        }

        /// <summary>
        /// 获取下一个目标
        /// </summary>
        /// <returns>图片地址</returns>
        private string Dequeue()
        {
            string Url = WaitUrlQueue.Dequeue().ToString();
            return Url;
        }


    }
}
