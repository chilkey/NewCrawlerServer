﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NetCrawlerCodeDemo
{
    /// <summary>
    /// BehaviorTemplate 行为模板
    /// </summary>
    public abstract class BehaviorTemplate
    {
        /// <summary>
        /// 总开关
        /// </summary>
        public bool Control { get; set; }

        /// <summary>
        /// 开始行动
        /// </summary>
        public abstract Task Action();
     
    }
}
