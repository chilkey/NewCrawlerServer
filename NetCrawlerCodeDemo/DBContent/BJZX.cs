﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetCrawlerCodeDemo.DBContent
{
    [Table(Name = "BJZX")]

    public class BJZX
    {
        [PrimaryKey, Identity]
        public int ID { get; set; }

        [Column(Name = "Url")]
        public string Url { get; set; }

        [Column(Name = "Title")]
        public string Title { get; set; }
        [Column(Name = "DocCode")]

        public string DocCode { get; set; }
        [Column(Name = "CreateTime")]
        public DateTime? CreateTime { get; set; }

        [Column(Name = "UrlLevel")]
        public int UrlLevel { get; set; }

        [Column(Name = "HasDetail")]
        public int HasDetail { get; set; }
    }
}
