﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetCrawlerCodeDemo.DBContent
{
    [Table(Name = "Syslogs")]
    public class Syslogs
    {
        [PrimaryKey, Identity]
        public int ID { get; set; }

        [Column(Name = "CreateTime")]
        public DateTime? CreateTime { get; set; }

        [Column(Name = "LogsContent")]
        public string LogsContent { get; set; }

        [Column(Name = "LogsType")]
        public int? LogsType { get; set; }

        [Column(Name = "Remark")]
        public string Remark { get; set; }
    }
}
