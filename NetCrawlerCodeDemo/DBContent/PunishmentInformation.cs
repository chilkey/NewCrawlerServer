﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetCrawlerCodeDemo.DBContent
{
    [Table(Name = "PunishmentInformation")]
   public  class PunishmentInformation
    {
        [PrimaryKey, Identity]
        public int ID { get; set; }

        /// <summary>
        /// 处罚人（单位）
        /// </summary>
        [Column(Name = "Title")]
        public string Title { get; set; }
        /// <summary>
        /// 公示时间
        /// </summary>
        [Column(Name = "Issue_date")]
        public string Issue_date { get; set; }
        /// <summary>
        /// 处罚决定文书号
        /// </summary>
        [Column(Name = "Article_id")]
        public string Article_id { get; set; }
        /// <summary>
        /// 办案机关
        /// </summary>
        [Column(Name = "Promulgator")]
        public string Promulgator { get; set; }
        /// <summary>
        /// 详情地址
        /// </summary>
        [Column(Name = "DetailUrl")]
        public string DetailUrl { get; set; }
        /// <summary>
        /// 警告信息
        /// </summary>
        [Column(Name = "WarningContent")]
        public string WarningContent { get; set; }
        /// <summary>
        /// 行政处罚决定书
        /// </summary>
        [Column(Name = "PenalizedBook")]
        public string PenalizedBook { get; set; }
        /// <summary>
        /// 更多企业信用信息地址
        /// </summary>
        [Column(Name = "MoreUrl")]
        public string MoreUrl { get; set; }
        /// <summary>
        /// 添加时间
        /// </summary>
        [Column(Name = "CreateTime")]
        public DateTime? CreateTime { get; set; }

        /// <summary>
        /// 上一次更新时间
        /// </summary>
        [Column(Name = "UpdateTime")]
        public DateTime? UpdateTime { get; set; }
        /// <summary>
        /// 是否爬取详情页面 0 否 1 是
        /// </summary>
        [Column(Name = "Isscanf")]
        public int? Isscanf { get; set; }
        /// <summary>
        /// 市   
        /// </summary>
        [Column(Name = "province")]
        public string province { get; set; }
        /// <summary>
        /// 区县
        /// </summary>
        [Column(Name = "city")]
        public string city { get; set; }

        /// <summary>
        /// 是否有效数据 0 否 1 是
        /// </summary>
        [Column(Name = "isenble")]
        public int? isenble { get; set; }

        /// <summary>
        /// 页面ID出现异常可以重新处理
        /// </summary>
        [Column(Name = "pageID")]
        public int? pageID { get; set; }

        /// <summary>
        /// 页面ID出现异常可以重新处理
        /// </summary>
        [Column(Name = "IspenalizedBook")]
        public int? IspenalizedBook { get; set; }
    }
}
