﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Text;

namespace NetCrawlerCodeDemo.DBContent
{
    [Table(Name = "BJZXDetail")]

    public class BJZXDetail
    {

        [PrimaryKey, Identity]
        public int ID { get; set; }

        [Column(Name = "WarningPublic")]
        public string WarningPublic { get; set; }

        [Column(Name = "Issue_date")]
        public string Issue_date { get; set; }

        [Column(Name = "Article_id")]
        public string Article_id { get; set; }

        [Column(Name = "Promulgator")]
        public string Promulgator { get; set; }

        [Column(Name = "PunishmentBook")]
        public string PunishmentBook { get; set; }

        [Column(Name = "CreateTime")]
        public DateTime? CreateTime { get; set; }

        [Column(Name = "Url")]
        public string Url { get; set; }

        [Column(Name = "UrlID")]
        public int? UrlID { get; set; }
    }
}
