﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetCrawlerCodeDemo.DBContent
{
    public class Common
    {
    }
    public class Text {
        public int ID { get; set; }
        public string Value { get; set; }
        public string Key { get; set; }
        public string Url { get; set; }
    }
    public class BasePage {
        public int pageNo { get; set; }
        public int pageSize { get; set; }
    }

    public class RequestBody : BasePage
    {
        public string entNameAddress { get { return ""; } set { } }
        public string issue_date { get { return ""; } set { } }
        public string vchr_bmmc { get { return ""; } set { } }
        public string before_year_3 { get { return "2015-01-22"; } set { } }
        public string issue_org_code { get { return ""; } set { } }
        public string deliv_date { get { return ""; } set { } }
        public string qy_ent_name { get { return ""; } set { } }
        public string qy_ent_no { get { return ""; } set { } }
        public string blsfxx { get { return ""; } set { } }
        public string num { get { return ""; } set { } }
        public string party_kind { get { return ""; } set { } }
        public int SelectPageSize { get; set; }//EntryPageNo
        public string EntryPageNo { get { return ""; } set { } }
        public string clear { get { return ""; } set { } }      
    }

    public class UrlQuery
    {
        public int ID { get; set; }
        public string Url  { get; set; }
        public int HasDetail { get; set; }
    }

    /// <summary>
    /// 行政处罚类
    /// </summary>
    public class Punish
    {
        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 详细Url
        /// </summary>
        public string DetailUrl { get; set; }
        /// <summary>
        /// 处决文书号
        /// </summary>
        public string Article_id { get; set; }
        /// <summary>
        /// 公示时间
        /// </summary>
        public string Issue_date { get; set; }
        //Promulgator
        /// <summary>
        /// 办案机关
        /// </summary>
        public string Promulgator { get; set; }
    }

    /// <summary>
    /// 行政处罚类
    /// </summary>
    public class PunishDetail
    {
        /// <summary>
        /// 警告内容
        /// </summary>
        public string WarningContent { get; set; }
        /// <summary>
        /// 行政处罚决定书
        /// </summary>
        public string PenalizedBook { get; set; }

        /// <summary>
        /// 行政处罚决定书
        /// </summary>
        public string MoreUrl { get; set; }

    }
}
