﻿using NetCrawlerCodeDemo.CommonHelp;
using NetCrawlerCodeDemo.DBContent;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using LinqToDB.Data;
using LinqToDB.Metadata;
using System.Linq;
using LinqToDB;
using System.Text;
using System.Text.RegularExpressions;
using System.Net.Http;
using System.IO;
using System.Net.Mail;
using System.Net;
using System.Net.Sockets;

namespace NetCrawlerCodeDemo
{
    class Program
    {
        static int TastSize = 10;
        static void Main(string[] args)
        {
            TaskSupervisor t = new TaskSupervisor(TastSize);
            for (int i = 0; i < TastSize; i++)
            {
                // t.Add(Task.Factory.StartNew(CrawlerBusiness.getAdministrativeSanctionDetail, i)); 
                //t.Add(Task.Factory.StartNew(CrawlerBusiness.getAdministrativeSanction, i));
                t.Add(Task.Factory.StartNew(CrawlerBusiness.getAdministrative, i));
            }
            t.StartAll();
            //string url = "http://qyxy.baic.gov.cn/newChange/newChangeAction!cfxx_view.dhtml?reg_bus_ent_id=a1a1a1a023c10aa60123c6acc6861922&chr_id=bj_fzc_fs_fzk_lx__gengdongming__20151111__23177198&info_type=040178&qy_reg_no=null";
            //string url = "http://qyxy.baic.gov.cn/newChange/newChangeAction!gsmd_list.dhtml?clear=true&flag_num=2";
            //string url = "http://qyxy.baic.gov.cn//newChange/newChangeAction!gscfws.dhtml?chr_id=bj_fzc_fs_fzk_lx__gengdongming__20151111__23177198&timeStamp";
            //var content = new NetCrawlerContent().GetURLContent(url);
            //string penalizedPattern = "<p class=p0 [^\\000]*?>\\s*([^\\000]*?)</p>";
            //contentHelp();

            //var str =  Read(@"F:\error2.txt");         
            // using (var db = new SqlDBContent())
            // {
            // var models = db.PunishmentInformation.Where(a=>a.Isscanf==1 & a.Title.Equals("央广诚品购物（北京）有限公司"));               
            // XmlHelper.CreateXmlByString(models.ToList());               
            //   }
            //updatebook();   
            //sendMeg();

            //SendMail("243814991@qq.com", "测试一下", "测试一下");
            //GetIP();
            Console.ReadKey();
        }
        static void updatebook()
        {
            using (var db = new SqlDBContent())
            {
                var models = db.PunishmentInformation.Where(a => a.Isscanf == 1).ToList();
                for (int i = 0; i < models.Count(); i++)
                {
                    if (models[i].PenalizedBook != null)
                    {
                        models[i].PenalizedBook = models[i].PenalizedBook.Replace("\t", "").Replace("\r\n", "");
                        db.PunishmentInformation.Where(a => a.ID == models[i].ID)
                            .Set(a => a.PenalizedBook, models[i].PenalizedBook)
                            .Update();
                    }
                }
            }
        }
        /// <summary>
        /// hskrfvsorjymdfja
        /// </summary>
        /// <param name="mailAddress"></param>
        /// <param name="mailTitle"></param>
        /// <param name="mailContent"></param>
        static void SendMail(string mailAddress, string mailTitle, string mailContent)
        {
            MailMessage objMailMessage = new MailMessage();
            string fromAddress = "2778705910@qq.com";//你在web.config中配置的发件人地址，就是你的邮箱地址。
            string mailHost = "smtp.qq.com";//邮件服务器/mail.qq.com
            objMailMessage.From = new MailAddress(fromAddress);//发送方地址
            objMailMessage.To.Add(new MailAddress(mailAddress));//收信人地址
            objMailMessage.BodyEncoding = Encoding.UTF8;//邮件编码
            objMailMessage.Subject = mailTitle;//邮件标题
            objMailMessage.Body = mailContent;//邮件内容
            objMailMessage.IsBodyHtml = true;//邮件正文是否为html格式
            SmtpClient objSmtpClient = new SmtpClient(mailHost, 587);
            objSmtpClient.UseDefaultCredentials = false;
            objSmtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;//通过网络发送到stmp邮件服务器
            objSmtpClient.Credentials = new System.Net.NetworkCredential("2778705910@qq.com", "hskrfvsorjymdfja");//发送方的邮件地址，授权码
            objSmtpClient.EnableSsl = true;//SMTP 服务器要求安全连接需要设置此属性           
            //objSmtpClient.
            try
            {
                objSmtpClient.Send(objMailMessage);
            }
            catch (Exception e)
            {
                Console.Out.WriteLine(e.Source + e.Message);
            }
        }

        static void GetIP()
        {
            string name = Dns.GetHostName();
            IPAddress[] ipadrlist = Dns.GetHostAddresses(name);
            foreach (IPAddress ipa in ipadrlist)
            {
                if (ipa.AddressFamily == AddressFamily.InterNetwork)
                    Console.Write(ipa.ToString());
            }
        }
    }
}
