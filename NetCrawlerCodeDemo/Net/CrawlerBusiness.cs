﻿using LinqToDB.Data;
using NetCrawlerCodeDemo.CommonHelp;
using NetCrawlerCodeDemo.DBContent;
using System;
using System.Collections.Generic;
using System.Text;
using LinqToDB;
using System.Linq;

namespace NetCrawlerCodeDemo
{
    /// <summary>
    /// 处理爬取内容业务逻辑类
    /// </summary>
    public class CrawlerBusiness
    {
        /// <summary>
        /// 分页大小
        /// </summary>
        private static int pageSize = 500;

        private static int querytimes = 32;

        private static int taskcounts = 10;
        /// <summary>
        /// 详情页Url队列大小
        /// </summary>
        private static int querySize = 500;
        /// <summary>
        /// 列表URL
        /// </summary>
        private static string baseUrl = @"http://qyxy.baic.gov.cn/newChange/newChangeAction!gsmd_list.dhtml?clear=true&flag_num=2";
        /// <summary>
        /// 主机地址
        /// </summary>        
        private static string hostUrl = @"http://qyxy.baic.gov.cn";

        /// <summary>
        /// table标签
        /// </summary>        
        private static string tablePattern = "<table cellspacing=\"0\" border=0 style=\"width: 790px;border:none;border-top:1px dashed #ccc;padding-top:10px;\" id=\"innerTable\">([^\\000]*?)</table>";

        /// <summary>
        /// 获取详细页面Url正则表达式
        /// </summary>
        private static string detailUrlPattern = "<span style=\"font-size:16px;color:blue;cursor:pointer\" onclick=\"showDialog\\('/([^']*)', '警示信息公示详细', '[^ ']*'\\)\" style=\"color:blue;font-size:16px;\">";

        /// <summary>
        /// 获取页面title正则表达式
        /// </summary>
        private static string titlePattern = "<font style=\"font-size:16px;color:blue;text-decoration: underline;\">([^\000]*?)</font>";

        /// <summary>
        /// 获取页面文书号正则表达式
        /// </summary>
        private static string articlePattern = "<td style=\"text-align:left;\">处罚决定书文号：([^号]*号)</td>";
        /// <summary>
        /// 获取页面办案机关正则表达式
        /// </summary>
        private static string promulgatorPattern = "<td style=\"text-align:left;\">办案机关：([^\000]*?)</td>";

        /// <summary>
        /// 获取处罚日期正则表达式
        /// </summary>
        private static string issue_datePattern = "<td style=\"text-align:left;\">公示时间：([^\000]*?)</td>";

        /// <summary>
        /// 获取详情警告信息正则表达式
        /// </summary>
        private static string warningPattern = "id=\"tableID\">([^\\000]*?)查看更多企业信用信息</p>";
        /// <summary>
        /// 获取详情行政处罚决定书正则表达式
        /// </summary>
        private static string penalizedPattern = "<div class=\"p0\"[^\\000]*?>\\s*([^\\000]*?)</body>";
        /// <summary>
        /// 企业更多信息
        /// </summary>
        private static string moreCompanyPattern = "<p onclick=\"showDialog\\('/([^']*)', '企业详细'";
        /// <summary>
        /// 
        /// </summary>
        private static string penalizedIDPattern = "chr_id = '\\s*([^\\000]*?)';//主键id";
        /// <summary>
        /// 线程等待时间
        /// </summary>
        private static int intThreadSleep = 30 * 1000;
        /// <summary>
        /// 爬取行政处决列表 根据任务ID 改变页码 爬取同时爬取多个页面数据
        /// </summary>
        /// <param name="o">任务ID</param>
        public static void getAdministrativeSanction(object o)
        {
            ///构造页面Post表单数据 
            RequestBody basePage = new RequestBody();
            basePage.pageSize = pageSize;
            basePage.SelectPageSize = pageSize;
            basePage.pageNo = pageSize * Convert.ToInt32(o) + 1;
            //爬取整个页面数据
            /* var content = new NetCrawlerContent().GetURLContent(baseUrl, basePage);
             //正则表达式匹配需要的数据
             var li = ConentHelper.GetContentUrl(content, detailUrlPattern, titlePattern, articlePattern, promulgatorPattern,issue_datePattern);

             List<PunishmentInformation> list = new List<PunishmentInformation>();
             for (int j = 0; j < li.Count; j++)
             {
                 list.Add(new PunishmentInformation()
                 {
                     CreateTime = DateTime.Now,
                     Title = li[j].Title,
                     DetailUrl = hostUrl + "/" + li[j].DetailUrl,
                     Article_id = li[j].Article_id,
                     Isscanf = 0,
                 });

             }
             using (var dbcontent = new SqlDBContent())
             {
                 //批量插入数据库      
                 dbcontent.BulkCopy(list);
             }
             */

        }
        /// <summary>
        /// 建议开启一个任务
        /// </summary>
        /// <param name="o">任务ID</param>
        public static void getAdministrativeSanctionDetail(object o)
        {

            Queue<UrlQuery> q = new Queue<UrlQuery>();
            ///获取数据库链接
            using (var dbcontent = new SqlDBContent())
            {
                var d = (from l in dbcontent.PunishmentInformation
                         where l.Isscanf == 0 & !l.IspenalizedBook.HasValue
                         select new UrlQuery
                         {
                             ID = l.ID,
                             Url = l.DetailUrl,
                         }).ToList();
                int i = (int)o;
                for (int j = 0; j < d.Count; j++)
                {
                    //if (d[j].ID % 20 == i)
                    if (j < querySize)
                    {
                        q.Enqueue(d[j]);
                        dbcontent.PunishmentInformation.Where(a => a.ID == d[j].ID)
                            .Set(a => a.IspenalizedBook, 2)
                            .Update();
                    }
                    else
                    {
                        break;
                    }
                }
            }          
            Counter counter = new Counter();
            DateTime dateTime = DateTime.Now;
            while (q.Count > 0)
            {
                var t = q.Dequeue();
                //计数器+1
                counter.Increment();
                System.Threading.Thread.Sleep(intThreadSleep);
                if (counter.Count > 30)
                {
                    //重置计数器
                    counter.InitCounter();
                    var sleepLong = _getTimeSpan(dateTime.AddHours(1)) + 1000;
                    System.Threading.Thread.Sleep((int)sleepLong);
                    dateTime = DateTime.Now;
                }
                string url = t.Url;
                var content = new NetCrawlerContent().GetURLContent(url);
                var li = ConentHelper.GetContentDetail(content, warningPattern, penalizedIDPattern, moreCompanyPattern);

                if (li.Count > 0)
                {
                    if (!string.IsNullOrWhiteSpace(li[0].PenalizedBook))
                    {
                        li[0].PenalizedBook = GetPenalizedBook(li[0].PenalizedBook);
                    }
                    using (var db = new SqlDBContent())
                    {
                        int one = db.PunishmentInformation.Where(a => a.ID == t.ID)
                            .Set(a => a.Isscanf, 1)
                            .Set(a => a.UpdateTime, DateTime.Now)
                            .Set(a => a.WarningContent, li[0].WarningContent)
                            .Set(a => a.PenalizedBook, li[0].PenalizedBook)
                            .Set(a => a.MoreUrl, "http://qyxy.baic.gov.cn/" + li[0].MoreUrl)
                            .Set(a => a.province, "北京")
                            .Set(a => a.IspenalizedBook, 0)
                            .Update();
                    }
                }
                else
                {
                    using (var db = new SqlDBContent())
                    {
                        int one = db.PunishmentInformation.Where(a => a.ID == t.ID)
                            .Set(a => a.Isscanf, 1)
                            .Set(a => a.UpdateTime, DateTime.Now)
                            .Update();
                    }
                }
            }
            //当一个队列数据爬去完成 又重新开始下一个队列爬取
            getAdministrativeSanctionDetail(o);
        }


        private static string GetPenalizedBook(string penalizedID)
        {
            string result = string.Empty;
            try
            {
                string url = "http://qyxy.baic.gov.cn/newChange/newChangeAction!gscfws.dhtml?chr_id=" + penalizedID;
                var content = new NetCrawlerContent().GetURLContent(url);
                content = content.Replace("</p>", "@");
                content = content.Replace(" ", "");
                content = content.Replace("@@", "@");
                content = HtmlHelper.NoHTML(content);
                content = content.Replace("\t", "").Replace("\r\n", "");
                result = content;
            }
            catch
            {

            }
            return result;
        }

        /// <summary>
        ///计算线程等待时间
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        private static double _getTimeSpan(DateTime date)
        {
            DateTime dateTime = DateTime.Now;
            var dtSpan = date - dateTime;
            return dtSpan.TotalMilliseconds > 0 ? dtSpan.TotalMilliseconds : 0;
        }

        public static void getAdministrative(object o)
        {
            ///构造页面Post表单数据 
            RequestBody basePage = new RequestBody();
            basePage.pageSize = pageSize;
            basePage.SelectPageSize = pageSize;
            basePage.pageNo = getPageNo(Convert.ToInt32(o));
            //爬取整个页面数据
            var content = new NetCrawlerContent().GetURLContent(baseUrl, basePage);
            //正则表达式匹配需要的数据
            var tablelist = ConentHelper.GetContentTable(content, tablePattern);

            var li = ConentHelper.GetListpatter(tablelist, detailUrlPattern, titlePattern, articlePattern, promulgatorPattern, issue_datePattern);

            List<PunishmentInformation> list = new List<PunishmentInformation>();
            for (int j = 0; j < li.Count; j++)
            {
                list.Add(new PunishmentInformation()
                {
                    CreateTime = DateTime.Now,
                    Title = li[j].Title,
                    DetailUrl = hostUrl + "/" + li[j].DetailUrl,
                    Article_id = li[j].Article_id,
                    Promulgator = li[j].Promulgator,
                    Isscanf = 0,
                    isenble = string.IsNullOrWhiteSpace(li[j].Article_id) ? 0 : 1,
                    pageID = getPageNo(Convert.ToInt32(o)),
                    Issue_date = li[j].Issue_date

                });

            }
            using (var dbcontent = new SqlDBContent())
            {
                //批量插入数据库      
                dbcontent.BulkCopy(list);
            }

        }

        private static int getPageNo(int taskID)
        {
            return  taskID + 1+ querytimes* taskcounts;
        }

    }
}
