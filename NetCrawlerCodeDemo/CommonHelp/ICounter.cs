﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NetCrawlerCodeDemo.CommonHelp
{
    interface ICounter
    {
        void Increment();
        void Decrement();
        void InitCounter();
    }
}
