﻿using NetCrawlerCodeDemo.DBContent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace NetCrawlerCodeDemo.CommonHelp
{
    public class ConentHelper
    {
        /// <summary>
        /// 正则表达式匹配列表页面
        /// </summary>
        /// <param name="content"></param>
        /// <param name="pattern1"></param>
        /// <param name="pattern2"></param>
        /// <param name="pattern3"></param>
        /// <param name="pattern4"></param>
        /// <returns></returns>
        private static List<Punish> GetContentUrl(string content, string pattern1, string pattern2, string pattern3, string patter4, string patter5)
        {
            List<Punish> list = new List<Punish>();
            ///Url
            Regex reg1 = new Regex(pattern1);
            ///Title
            Regex reg2 = new Regex(pattern2);
            ///文书号
            Regex reg3 = new Regex(pattern3);
            ///办案机关
            Regex reg4 = new Regex(patter4);
            ///处罚日期
            Regex reg5 = new Regex(patter5);
            MatchCollection match1 = reg1.Matches(content);
            MatchCollection match2 = reg2.Matches(content);
            MatchCollection match3 = reg3.Matches(content);
            MatchCollection match4 = reg4.Matches(content);
            MatchCollection match5 = reg5.Matches(content);

            list.Add(new Punish()
            {
                DetailUrl = match1.Count > 0 ? match1[0].Groups.Count > 0 ? match1[0].Groups[1].Value : "" : "",
                Title = match2.Count > 0 ? match2[0].Groups.Count > 0 ? match2[0].Groups[1].Value : "" : "",
                Article_id = GetArticleidByContent(content),//match3.Count > 0 ? (match3[0].Groups.Count > 1 ? match3[0].Groups[1].Value : "") : "",
                Promulgator = match4.Count > 0 ? match4[0].Groups.Count > 0 ? match4[0].Groups[1].Value : "" : "",
                Issue_date = GetIssue_dateByContent(content)// match5.Count > 0 ? match5[0].Groups.Count > 0 ? match5[0].Groups[1].Value : "" : "",
            });

            return list;
        }

        private static string GetArticleidByContent(string content)
        {
            try
            {
                var sp = content.Split("处罚决定书文号：");
                var spcontent = sp[1];
                return spcontent.Substring(0, spcontent.IndexOf("</td>"));
            }
            catch
            {
                return string.Empty;
            }
        }
        private static string GetIssue_dateByContent(string content)
        {
            try
            {
                var sp = content.Split("公示时间：");
                var spcontent = sp[1];
                return spcontent.Substring(0, spcontent.IndexOf("</td>"));
            }
            catch
            {
                return string.Empty;
            }
        }
        /// <summary>
        /// 正则表达式匹配详情页面
        /// </summary>
        /// <param name="content"></param>
        /// <param name="pattern1"></param>
        /// <param name="pattern2"></param>
        /// <returns></returns>
        public static List<PunishDetail> GetContentDetail(string content, string pattern1, string pattern2, string pattern3)
        {
            List<PunishDetail> list = new List<PunishDetail>();

            //警告信息
            Regex reg1 = new Regex(pattern1);
            //处罚决定书
            Regex reg2 = new Regex(pattern2);
            //更多企业征信信息
            Regex reg3 = new Regex(pattern3);
            MatchCollection match1 = reg1.Matches(content);
            MatchCollection match2 = reg2.Matches(content);
            MatchCollection match3 = reg3.Matches(content);

            list.Add(new PunishDetail()
            {
                WarningContent = match1.Count > 0 ? match1[0].Groups.Count > 0 ? match1[0].Groups[1].Value : "" : "",
                PenalizedBook = match2.Count > 0 ? (match2[0].Groups.Count > 0 ? match2[0].Groups[1].Value : "") : "",
                MoreUrl = match3.Count > 0 ? (match3[0].Groups.Count > 0 ? match3[0].Groups[1].Value : "") : "",
            });
            return list;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="pattern"></param>
        /// <returns></returns>
        public static List<string> GetContentTable(string content, string pattern)
        {
            List<string> result = new List<string>();

            ///Url
            Regex reg1 = new Regex(pattern);
            MatchCollection match1 = reg1.Matches(content);
            for (int i = 0; i < match1.Count; i++)
            {
                result.Add(match1[i].Groups.Count > 0 ? match1[i].Groups[1].Value : "");
            }
            return result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="content"></param>
        /// <param name="pattern1"></param>
        /// <param name="pattern2"></param>
        /// <param name="pattern3"></param>
        /// <param name="patter4"></param>
        /// <returns></returns>
        public static List<Punish> GetListpatter(List<string> content, string pattern1, string pattern2, string pattern3, string pattern4, string pattern5)
        {
            List<Punish> list = new List<Punish>();

            for (int i = 0; i < content.Count; i++)
            {
                list.AddRange(GetContentUrl(content[i], pattern1, pattern2, pattern3, pattern4, pattern5));
            }
            return list;
        }
    }
}
