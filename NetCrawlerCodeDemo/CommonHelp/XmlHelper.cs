﻿using NetCrawlerCodeDemo.DBContent;
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace NetCrawlerCodeDemo.CommonHelp
{
    public class XmlHelper
    {
        /// <summary>
        /// 采用xml方式生成
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public static string CreateXmlDoc(PunishmentInformation model)
        {
            model = CreateNewModel(model);
            XmlText xmltext;
            XmlDocument xmldoc = new XmlDocument();
            //加入XML的声明段落
            XmlNode xmlnode = xmldoc.CreateXmlDeclaration("1.0", "UTF-8", null);
            xmldoc.AppendChild(xmlnode);
            //创建data节点
            XmlElement xmldata = xmldoc.CreateElement("", "data", "");
            xmltext = xmldoc.CreateTextNode("");
            xmldata.AppendChild(xmltext);
            xmldoc.AppendChild(xmldata);
            //创建doc节点
            XmlElement xmldoctext = xmldoc.CreateElement("", "doc", "");
            xmltext = xmldoc.CreateTextNode("");
            xmldoctext.AppendChild(xmltext);
            xmldoc.ChildNodes.Item(1).AppendChild(xmldoctext);

            //创建title节点
            XmlElement xmltitle = xmldoc.CreateElement("", "title", "");
            xmltext = xmldoc.CreateTextNode(model.Title);
            xmltitle.AppendChild(xmltext);
            xmldoc.ChildNodes.Item(1).AppendChild(xmldoctext).AppendChild(xmltitle);

            //创建content节点
            XmlElement xmlcontent = xmldoc.CreateElement("", "content", "");
            xmltext = xmldoc.CreateTextNode(string.Format(@"<![CDATA[{0}{1}]]> ", model.WarningContent, model.PenalizedBook));
            xmlcontent.AppendChild(xmltext);
            xmldoc.ChildNodes.Item(1).AppendChild(xmldoctext).AppendChild(xmlcontent);
            //创建issue_date节点
            XmlElement xmlissue_date = xmldoc.CreateElement("", "issue_date", "");
            xmltext = xmldoc.CreateTextNode(string.Format("{0}", model.Issue_date));
            xmlissue_date.AppendChild(xmltext);
            xmldoc.ChildNodes.Item(1).AppendChild(xmldoctext).AppendChild(xmlissue_date);
            //创建article_id节点
            XmlElement xmlarticle_id = xmldoc.CreateElement("", "article_id", "");
            xmltext = xmldoc.CreateTextNode(string.Format("{0}", model.Article_id));
            xmlarticle_id.AppendChild(xmltext);
            xmldoc.ChildNodes.Item(1).AppendChild(xmldoctext).AppendChild(xmlarticle_id);
            //创建district节点
            XmlElement xmldistrict = xmldoc.CreateElement("", "district", "");
            xmltext = xmldoc.CreateTextNode("");
            xmldistrict.AppendChild(xmltext);
            xmldoc.ChildNodes.Item(1).AppendChild(xmldoctext).AppendChild(xmldistrict);
            //创建province节点
            XmlElement xmlprovince = xmldoc.CreateElement("", "province", "");
            xmltext = xmldoc.CreateTextNode(string.Format("{0} ", model.province));
            xmlprovince.AppendChild(xmltext);
            xmldistrict.AppendChild(xmlprovince);
            //创建city节点
            XmlElement xmlcity = xmldoc.CreateElement("", "city", "");
            xmltext = xmldoc.CreateTextNode(string.Format("{0} ", model.city));
            xmlcity.AppendChild(xmltext);
            xmldistrict.AppendChild(xmlcity);
            //创建promulgator节点
            XmlElement xmlpromulgator = xmldoc.CreateElement("", "promulgator", "");
            xmltext = xmldoc.CreateTextNode(string.Format("{0} ", model.Promulgator));
            xmlpromulgator.AppendChild(xmltext);
            xmldoc.ChildNodes.Item(1).AppendChild(xmldoctext).AppendChild(xmlpromulgator);
            //创建from节点
            XmlElement xmlfrom = xmldoc.CreateElement("", "from", "");
            xmltext = xmldoc.CreateTextNode(string.Format("{0} ", model.DetailUrl));
            xmlfrom.AppendChild(xmltext);
            xmldoc.ChildNodes.Item(1).AppendChild(xmldoctext).AppendChild(xmlfrom);
            xmldoc.Save(string.Format(@"F:\beijingXML\{0}.xml", model.Title.Replace("*", ""))); //保存 

            return string.Format(@"F:\beijingXML\{0}.xml", model.Title);
        }
        /// <summary>
        /// 组装有效数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        private static PunishmentInformation CreateNewModel(PunishmentInformation model)
        {
            string str = model.WarningContent;           
            int index = str.IndexOf("</table>");
            if (index > -1)
            {
                str = str.Substring(0, index);
            }
            var str1 = HtmlHelper.NoHTML(str);
            var list1 = str1.Split('@');
            Dictionary<string, string> d = HtmlHelper.mathUseContent(list1);
            StringBuilder sb = new StringBuilder();
            foreach (var k in d)
            {
                sb.AppendFormat("{0}:{1}\r\n", k.Key, k.Value);
            }
            model.WarningContent = sb.ToString();
            model.PenalizedBook = getPenalizedBook(model.PenalizedBook);
            model.city = getCity(model.Promulgator);
            return model;
        }

        /// <summary>
        /// 采用文本方式生成Xml
        /// </summary>
        /// <param name="model"></param>
        public static void CreateXmlByString(List<PunishmentInformation> model)
        {
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat("<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n");
            sb.AppendFormat("<data>");
            foreach (var m in model)
            {
                sb.Append(CreateXmlByString(m));
            }
            sb.AppendFormat("\r\n</data>");
            System.IO.File.WriteAllText(string.Format(@"F:\beijingXML\{0}.xml", DateTime.Now.Ticks), sb.ToString());
        }
        private static string CreateXmlByString(PunishmentInformation model)
        {
            model = CreateNewModel(model);
            StringBuilder sb = new StringBuilder();
            sb.AppendFormat(@"
  <doc>
     <title>{0}</title >
     <content><![CDATA[{1}{2}]]></content>
     <issue_date>{3}</issue_date >
     <article_id>{4}</article_id >
     <district>
       <province>{5}</province >
       <city>{6}</city>
     </district>
     <promulgator>{7}</promulgator>
     <from>{8}</from>
  </doc>", model.Title, model.WarningContent, model.PenalizedBook, model.Issue_date, model.Article_id, model.province, model.city, model.Promulgator, model.DetailUrl.Replace("&", "&amp;"));

            return sb.ToString();
        }

        /// <summary>
        /// 获取区
        /// </summary>
        /// <param name="Promulgator"></param>
        /// <returns></returns>
        private static string getCity(string Promulgator)
        {
            string city = string.Empty;
            //区列表
            var cityList = new List<string>() { "东城", "西城", "朝阳", "丰台", "石景山", "海淀", "门头沟", "房山", "通州", "顺义", "昌平", "大兴", "怀柔", "平谷", "密云", "延庆" };
            foreach (var v in cityList)
            {
                if (Promulgator.Contains(v))
                {
                    city = v + "区";
                }
            }
            return city;
        }
        /// <summary>
        /// 获取PenalizedBook
        /// </summary>
        /// <param name="PenalizedBook"></param>
        /// <returns></returns>
        private static string getPenalizedBook(string PenalizedBook)
        {
            string retult = string.Empty;
            if (string.IsNullOrWhiteSpace(PenalizedBook))
            {
                return retult;
            }
            var strlist = PenalizedBook.Replace("     ", "@").Replace(" ","").Split("@");
            foreach (var v in strlist)
            {
                if (!string.IsNullOrWhiteSpace(v.Trim()))
                    retult += v.Trim().Replace("br", "\r\n") + "\r\n";
            }
            return retult;
        }
    }
}
