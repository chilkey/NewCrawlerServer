﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace NetCrawlerCodeDemo
{
    public class HtmlHelper
    {
        /// <summary>
        /// 去除HTML标记   
        /// </summary>
        /// <param name="Htmlstring"></param>
        /// <returns></returns>
        public static string NoHTML(string Htmlstring)
        {
            if (string.IsNullOrWhiteSpace(Htmlstring))
            {
                return "";
            }
            Htmlstring = Htmlstring.Replace("</tr>", "@");
            //删除脚本   
            Htmlstring = Regex.Replace(Htmlstring, @"<script[^>]*?>.*?</script>", "", RegexOptions.IgnoreCase);
            //删除HTML   
            Htmlstring = Regex.Replace(Htmlstring, @"<(.[^>]*)>", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"([/r/n])[/s]+", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"-->", "", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"<!--.*", "", RegexOptions.IgnoreCase);

            Htmlstring = Regex.Replace(Htmlstring, @"&(quot|#34);", "\"", RegexOptions.IgnoreCase);

            Htmlstring = Regex.Replace(Htmlstring, @"&(amp|#38);", "&", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(lt|#60);", "<", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(gt|#62);", ">", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(nbsp|#160);", " ", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(iexcl|#161);", "/xa1", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(cent|#162);", "/xa2", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(pound|#163);", "/xa3", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&(copy|#169);", "/xa9", RegexOptions.IgnoreCase);
            Htmlstring = Regex.Replace(Htmlstring, @"&#(/d+);", "", RegexOptions.IgnoreCase);

            Htmlstring.Replace("<", "");
            Htmlstring.Replace(">", "");
            Htmlstring.Replace("/r/n", "");
            //Htmlstring = HttpContent.Current.Server.HtmlEncode(Htmlstring).Trim();

            return Htmlstring;
        }
        /// <summary>
        /// 正则表达式匹配内容
        /// </summary>
        /// <param name="Htmlstring"></param>
        /// <param name="detailUrlPattern"></param>
        /// <returns></returns>
        public static MatchCollection patternString(string Htmlstring, string detailUrlPattern)
        {
            Regex reg = new Regex(detailUrlPattern);
            var result = reg.Matches(Htmlstring);
            return result;
        }
        /// <summary>
        /// 匹配拆分键值对
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        public static Dictionary<string, string> mathUseContent(IList<string> list)
        {
            Dictionary<string, string> d = new Dictionary<string, string>();
            foreach (var l in list)
            {
                var list2 = l.Replace("：", ";").Split(';');
                if (string.IsNullOrWhiteSpace(list2[0]))
                {
                    continue;
                }
                if (list2.Length >= 2)
                {
                    string value = string.Empty;
                    for (int i = 1; i < list2.Length; i++)
                    {
                        value += list2[i].Trim();
                    }
                    d.Add(list2[0].Trim(), list2[1].Trim());
                }
                else if (list2.Length == 1)
                {
                    d.Add(list2[0].Trim(), "");
                }            
            }
            return d;
        }

    }
}
